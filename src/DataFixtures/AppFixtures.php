<?php

namespace App\DataFixtures;

use App\Entity\Book;
use App\Entity\Editor;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    public $encoder;
    function __construct(UserPasswordEncoderInterface $upe)
    {
        $this->encoder = $upe;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $user1 = new User();
        $user1->setUsername('Utilisateur de base');
        $user1->setEmail('admin@admin.com');
        $user1->setPassword($this->encoder->encodePassword($user1, 'admin123'));
        $user1->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        $manager->persist($user1);

        $editor1 = new Editor();
        $editor1->setName('Glénat');
        $editor1->setDescription('Un éditeur prolifique qui fut l\'un des premier à publier des mangas dans l\'héxagone ');
        $manager->persist($editor1);

        $editor2 = new Editor();
        $editor2->setName('Delcourt');
        $editor2->setDescription('Un éditeur français');
        $manager->persist($editor2);

        $editor3 = new Editor();
        $editor3->setName("J'ai lu");
        $editor3->setDescription('Un éditeur qui revolutionna la diffusion grâce à des livres plus petit et de fabrication moins qualitative, mais bien meilleurs marchés');
        $manager->persist($editor3);

        $book1 = new Book();
        $book1->setTitle('Dragon Ball T. 1');
        $book1->setAbstract('Nous decouvrons San Goku le petit garçon à la queue de singe.');
        $book1->setImage('dragon_ball_1.jpg');
        $book1->setEditor($editor1);
        $book1->setCreatedAt(new \DateTime());
        $book1->setUpdatedAt(new \DateTime());
        $manager->persist($book1);

        $book10 = new Book();
        $book10->setTitle('Dragon Ball T. 2');
        $book10->setAbstract('Premier affrontement avec le demon Picollo.');
        $book10->setImage('dragon_ball_1.jpg');
        $book10->setEditor($editor1);
        $book10->setCreatedAt(new \DateTime());
        $book10->setUpdatedAt(new \DateTime());
        $manager->persist($book10);

        $book11 = new Book();
        $book11->setTitle('Dragon Ball T. 3');
        $book11->setAbstract("L'armée du Red Ribbon fait son entrée");
        $book11->setImage('dragon_ball_1.jpg');
        $book11->setEditor($editor1);
        $book11->setCreatedAt(new \DateTime());
        $book11->setUpdatedAt(new \DateTime());
        $manager->persist($book11);

        $book2 = new Book();
        $book2->setTitle('Danse Macabre');
        $book2->setAbstract("Plusieurs nouvelles du maitre de l\épouvante Stephen King");
        $book2->setImage('danse_macabre.jpg');
        $book2->setEditor($editor2);
        $book2->setCreatedAt(new \DateTime());
        $book2->setUpdatedAt(new \DateTime());
        $manager->persist($book2);

        $manager->flush();
    }
}
