<?php

namespace App\Controller;

use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/book")
 */
class BookController extends AbstractController
{
    
     /**
     * @Route("/", name="book_list")
     */
    public function book_list(BookRepository $br)
    {
        $books = $br->findAll();

        return $this->render('book/list.html.twig', [            
            'books' => $books,
        ]);
    }

    /**
     * @Route("/{id}", name="book_show")
     */
    public function book_show(BookRepository $br, $id)
    {
        $book = $br->find($id);

        return $this->render('book/show.html.twig', [            
            'book' => $book,
        ]);
    }
}
