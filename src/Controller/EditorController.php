<?php

namespace App\Controller;

use App\Repository\EditorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/editeur")
 */
class EditorController extends AbstractController
{
    /**
     * @Route("/", name="editor_list")
     */
    public function editor_list(EditorRepository $er)
    {
        $editors = $er->findAll();

        return $this->render('editor/list.html.twig', [            
            'editors' => $editors,
        ]);
    }

    /**
     * @Route("/{id}", name="editor_show")
     */
    public function editor_show(EditorRepository $er, $id)
    {
        $editor = $er->find($id);

        return $this->render('editor/show.html.twig', [            
            'editor' => $editor,
        ]);
    }
}
