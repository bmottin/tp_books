<?php

namespace App\Controller;

use App\Repository\AuthorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
     * @Route("/author")
     */
class AuthorController extends AbstractController
{
    /**
     * @Route("/list", name="author_list")
     */
    public function authorList(AuthorRepository $authorRepository)
    {

        $authors = $authorRepository->findAll();

        return $this->render('author/list.html.twig', [
            'authors' => $authors
        ]);
    }

    /**
     * @Route("/show/{id}", name="author_show")
     */
    public function authorShow(AuthorRepository $authorRepository, $id)
    {

        $author = $authorRepository->find($id);

        return $this->render('author/show.html.twig', [
            'author' => $author
        ]);
    }
}
