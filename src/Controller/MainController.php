<?php

namespace App\Controller;

use App\Repository\EditorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home()
    {

        return $this->render('main/index.html.twig', [            
        ]);
    }

  /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        // TODO faire page a propos
        return $this->render('main/about.html.twig', [            
        ]);
    }

      /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        // TODO faire page contact
        return $this->render('main/index.html.twig', [
        ]);
    }



}
